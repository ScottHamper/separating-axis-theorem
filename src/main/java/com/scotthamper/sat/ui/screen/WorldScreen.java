package com.scotthamper.sat.ui.screen;

import com.scotthamper.sat.model.Circle;
import com.scotthamper.sat.model.Input;
import com.scotthamper.sat.model.Polygon;
import com.scotthamper.sat.model.Vector2D;
import com.scotthamper.sat.model.World;
import com.scotthamper.sat.ui.ApplicationTimer;
import com.scotthamper.sat.ui.Fxml;
import com.scotthamper.sat.ui.input.KeyboardInputProvider;
import java.util.Map;
import java.util.function.ToDoubleFunction;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

public class WorldScreen extends VBox {
  private static final double PIXELS_PER_METER = 100.0;

  @FXML private Canvas canvas;

  private final World world;

  public WorldScreen() {
    var polygonInput =
        new KeyboardInputProvider(
            sceneProperty(),
            Map.of(
                Input.MoveUp, KeyCode.W,
                Input.MoveLeft, KeyCode.A,
                Input.MoveDown, KeyCode.S,
                Input.MoveRight, KeyCode.D));

    var circleInput =
        new KeyboardInputProvider(
            sceneProperty(),
            Map.of(
                Input.MoveUp, KeyCode.UP,
                Input.MoveLeft, KeyCode.LEFT,
                Input.MoveDown, KeyCode.DOWN,
                Input.MoveRight, KeyCode.RIGHT));

    world = new World(polygonInput, circleInput);

    Fxml.load(this);
  }

  @FXML
  private void initialize() {
    canvas.widthProperty().bind(widthProperty());
    canvas.heightProperty().bind(heightProperty());

    canvas.getGraphicsContext2D().setFill(Color.rgb(30, 30, 200, 0.5));

    new ApplicationTimer(world::update, this::render).start();
  }

  private void render() {
    GraphicsContext context = canvas.getGraphicsContext2D();
    context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

    context.save();

    // Translating by the canvas height moves (0, 0) to the bottom-left corner of the screen.
    // Scaling y coordinates by a negative amount causes the coordinate system to flip, making
    // positive y values be above (0, 0) rather than below.
    var translate = new Translate(0.0, canvas.getHeight());
    var scale = new Scale(PIXELS_PER_METER, -PIXELS_PER_METER);
    context.setTransform(new Affine(translate.createConcatenation(scale)));

    // The line width must be reduced to compensate for the scaling applied above.
    context.setLineWidth(1.0 / PIXELS_PER_METER);

    Polygon obstacle = world.obstacle();
    double[] obstacleXs = vertexComponents(obstacle, Vector2D::x);
    double[] obstacleYs = vertexComponents(obstacle, Vector2D::y);

    context.strokePolygon(obstacleXs, obstacleYs, obstacle.vertices().size());
    context.fillPolygon(obstacleXs, obstacleYs, obstacle.vertices().size());

    Circle movingCircle = world.movingCircle();
    context.strokeOval(
        movingCircle.center().x() - movingCircle.radius(),
        movingCircle.center().y() - movingCircle.radius(),
        movingCircle.diameter(),
        movingCircle.diameter());

    Polygon movingPolygon = world.movingPolygon();
    context.strokePolygon(
        vertexComponents(movingPolygon, Vector2D::x),
        vertexComponents(movingPolygon, Vector2D::y),
        movingPolygon.vertices().size());

    context.restore();
  }

  private double[] vertexComponents(Polygon polygon, ToDoubleFunction<Vector2D> extractor) {
    return polygon.vertices().stream().mapToDouble(extractor).toArray();
  }
}
