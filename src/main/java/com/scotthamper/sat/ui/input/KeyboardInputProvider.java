package com.scotthamper.sat.ui.input;

import com.scotthamper.sat.model.Input;
import com.scotthamper.sat.model.InputProvider;
import com.scotthamper.sat.model.Vector2D;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyboardInputProvider implements InputProvider {
  private final Map<Input, KeyCode> keymap;
  private final Set<KeyCode> keysPressed;

  public KeyboardInputProvider(ObservableValue<Scene> sceneProperty, Map<Input, KeyCode> keymap) {
    this.keymap = Map.copyOf(keymap);
    keysPressed = new HashSet<>();

    sceneProperty.addListener(
        (p, oldScene, newScene) -> {
          if (oldScene != null) {
            removeEventHandlers(oldScene);
          }

          if (newScene != null) {
            addEventHandlers(newScene);
          }
        });

    Scene scene = sceneProperty.getValue();

    if (scene != null) {
      addEventHandlers(scene);
    }
  }

  private void addEventHandlers(Scene scene) {
    scene.addEventHandler(KeyEvent.KEY_PRESSED, this::onKeyPressed);
    scene.addEventHandler(KeyEvent.KEY_RELEASED, this::onKeyReleased);
  }

  private void removeEventHandlers(Scene scene) {
    scene.removeEventHandler(KeyEvent.KEY_PRESSED, this::onKeyPressed);
    scene.removeEventHandler(KeyEvent.KEY_RELEASED, this::onKeyReleased);
  }

  private void onKeyPressed(KeyEvent event) {
    keysPressed.add(event.getCode());
  }

  private void onKeyReleased(KeyEvent event) {
    keysPressed.remove(event.getCode());
  }

  @Override
  public Vector2D movement() {
    double rightInput = keysPressed.contains(keymap.get(Input.MoveRight)) ? 1 : 0;
    double leftInput = keysPressed.contains(keymap.get(Input.MoveLeft)) ? 1 : 0;
    double upInput = keysPressed.contains(keymap.get(Input.MoveUp)) ? 1 : 0;
    double downInput = keysPressed.contains(keymap.get(Input.MoveDown)) ? 1 : 0;

    return new Vector2D(rightInput - leftInput, upInput - downInput);
  }
}
