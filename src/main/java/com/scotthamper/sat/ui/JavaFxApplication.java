package com.scotthamper.sat.ui;

import com.scotthamper.sat.ui.screen.WorldScreen;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class JavaFxApplication extends javafx.application.Application {
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) {
    var scene = new Scene(new WorldScreen());

    scene.rootProperty().addListener((p, o, n) -> stage.sizeToScene());

    stage.setTitle("Separating Axis Theorem");
    stage.setScene(scene);
    stage.show();
  }
}
