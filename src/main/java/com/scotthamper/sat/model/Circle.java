package com.scotthamper.sat.model;

public record Circle(Vector2D center, double radius) implements Shape {
  public double diameter() {
    return radius * 2;
  }

  public Circle translate(Vector2D delta) {
    return new Circle(center.add(delta), radius);
  }

  @Override
  public Range scalarProjection(Vector2D vector) {
    double centerProjection = center.scalarProjection(vector);
    return new Range(centerProjection - radius, centerProjection + radius);
  }
}
