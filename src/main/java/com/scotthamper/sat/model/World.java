package com.scotthamper.sat.model;

public class World {
  private static final double MOVEMENT_SPEED = 1.5; // meters per second

  private final InputProvider polygonInput;
  private final InputProvider circleInput;

  private final Polygon obstacle;
  private Polygon movingPolygon;
  private Circle movingCircle;

  public World(InputProvider polygonInput, InputProvider circleInput) {
    this.polygonInput = polygonInput;
    this.circleInput = circleInput;

    obstacle =
        new Polygon(
            new Vector2D(3.5, 4.25),
            new Vector2D(3.0, 4.05),
            new Vector2D(2.5, 3.05),
            new Vector2D(3.5, 2.25),
            new Vector2D(5.5, 2.25));

    movingPolygon =
        new Polygon(
            new Vector2D(4.5, 3.25),
            new Vector2D(3.5, 2.75),
            new Vector2D(4.5, 2.25),
            new Vector2D(5.5, 2.25),
            new Vector2D(5.5, 3.25));

    movingCircle = new Circle(new Vector2D(4.0, 4.0), .5);
  }

  public Polygon obstacle() {
    return obstacle;
  }

  public Polygon movingPolygon() {
    return movingPolygon;
  }

  public Circle movingCircle() {
    return movingCircle;
  }

  public void update(double timeDelta) {
    Vector2D polygonDelta = polygonInput.movement().scale(timeDelta * MOVEMENT_SPEED);
    movingPolygon = movingPolygon.translate(polygonDelta);
    movingPolygon = movingPolygon.translate(Collision.resolution(obstacle, movingPolygon));

    Vector2D circleDelta = circleInput.movement().scale(timeDelta * MOVEMENT_SPEED);
    movingCircle = movingCircle.translate(circleDelta);
    movingCircle = movingCircle.translate(Collision.resolution(obstacle, movingCircle));
  }
}
