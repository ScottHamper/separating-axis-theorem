package com.scotthamper.sat.model;

public record Range(double start, double end) {
  public Range {
    if (start > end) {
      throw new IllegalArgumentException();
    }
  }

  public double length() {
    return end - start;
  }

  public Range intersect(Range other) {
    double start = Math.max(this.start, other.start);
    double end = Math.min(this.end, other.end);

    if (start > end) {
      return new Range(0.0, 0.0);
    }

    return new Range(start, end);
  }
}
