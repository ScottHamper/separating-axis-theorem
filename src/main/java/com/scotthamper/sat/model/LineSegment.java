package com.scotthamper.sat.model;

public record LineSegment(Vector2D start, Vector2D end) {
  public Vector2D vector() {
    return end.subtract(start);
  }
}
