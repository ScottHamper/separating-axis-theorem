package com.scotthamper.sat.model;

import java.util.Iterator;

public final class Iterables {
  public static <T> Iterable<T> concat(Iterable<T> first, Iterable<T> second) {
    return () ->
        new Iterator<>() {
          private final Iterator<T> firstIterator = first.iterator();
          private final Iterator<T> secondIterator = second.iterator();

          @Override
          public boolean hasNext() {
            return firstIterator.hasNext() || secondIterator.hasNext();
          }

          @Override
          public T next() {
            return firstIterator.hasNext() ? firstIterator.next() : secondIterator.next();
          }
        };
  }
}
