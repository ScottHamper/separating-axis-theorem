package com.scotthamper.sat.model;

public record Vector2D(double x, double y) {
  public static final Vector2D ZERO = new Vector2D(0, 0);

  public double magnitude() {
    return Math.sqrt(dotProduct(this));
  }

  public Vector2D add(Vector2D other) {
    return new Vector2D(x + other.x, y + other.y);
  }

  public Vector2D subtract(Vector2D other) {
    return new Vector2D(x - other.x, y - other.y);
  }

  public Vector2D scale(double scale) {
    return new Vector2D(x * scale, y * scale);
  }

  public double dotProduct(Vector2D other) {
    return x * other.x + y * other.y;
  }

  public double scalarProjection(Vector2D other) {
    return dotProduct(other.unit());
  }

  public Vector2D unit() {
    return scale(1.0 / magnitude());
  }

  // Vectors actually have two normals, but there's currently no need to differentiate between them.
  public Vector2D normal() {
    return new Vector2D(y, -x);
  }
}
