package com.scotthamper.sat.model;

public interface Shape {
  Range scalarProjection(Vector2D vector);
}
