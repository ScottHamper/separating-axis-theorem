package com.scotthamper.sat.model;

public enum Input {
  MoveUp,
  MoveRight,
  MoveDown,
  MoveLeft,
}
