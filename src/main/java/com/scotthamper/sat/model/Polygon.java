package com.scotthamper.sat.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public record Polygon(List<Vector2D> vertices) implements Shape {
  private static final int MIN_VERTEX_COUNT = 3;

  public Polygon(List<Vector2D> vertices) {
    if (vertices.size() < MIN_VERTEX_COUNT) {
      throw new IllegalArgumentException();
    }

    this.vertices = List.copyOf(vertices);
  }

  public Polygon(Vector2D first, Vector2D second, Vector2D third, Vector2D... additional) {
    this(Stream.concat(Stream.of(first, second, third), Arrays.stream(additional)).toList());
  }

  public List<Vector2D> vertices() {
    return Collections.unmodifiableList(vertices);
  }

  public Polygon translate(Vector2D delta) {
    List<Vector2D> translatedVertices = vertices.stream().map(v -> v.add(delta)).toList();

    return new Polygon(translatedVertices);
  }

  // It would be nice to pre-compute all the edges at construction time, but record classes don't
  // allow instance variables beyond what's specified in the primary constructor. I don't want to
  // change Polygon back into a non-record class, so if I'm going to have to re-compute edges every
  // time the method is called, let's at least do it lazily.
  public Iterable<LineSegment> edges() {
    return () ->
        new Iterator<>() {
          private int index = 1;

          @Override
          public boolean hasNext() {
            return index <= vertices.size();
          }

          @Override
          public LineSegment next() {
            Vector2D start = vertices.get(index - 1);
            Vector2D end = vertices.get(index % vertices.size());

            index++;
            return new LineSegment(start, end);
          }
        };
  }

  @Override
  public Range scalarProjection(Vector2D vector) {
    List<Double> projections =
        vertices.stream().map(v -> v.scalarProjection(vector)).sorted().toList();

    return new Range(projections.get(0), projections.get(projections.size() - 1));
  }
}
