package com.scotthamper.sat.model;

import java.util.stream.StreamSupport;

public final class Collision {
  private static Iterable<Vector2D> separatingAxisCandidates(Polygon polygon) {
    return () ->
        StreamSupport.stream(polygon.edges().spliterator(), false)
            .map(lineSegment -> lineSegment.vector().normal())
            .iterator();
  }

  public static Vector2D resolution(Polygon first, Polygon second) {
    // FIXME: Potentially produces the same candidate twice. In addition to both shapes possibly
    //  returning the exact same candidate, there's the possibility that two distinct vectors end up
    //  being equivalent axis - we don't care about the magnitude of the axis vector, nor do we care
    //  if the vector is going "forward" or "backward". Will have to give up lazy evaluation of axes
    //  in order to solve this, however.
    Iterable<Vector2D> separatingAxisCandidates =
        Iterables.concat(separatingAxisCandidates(first), separatingAxisCandidates(second));

    return resolution(first, second, separatingAxisCandidates);
  }

  public static Vector2D resolution(Polygon polygon, Circle circle) {
    Iterable<Vector2D> separatingAxisCandidates =
        Iterables.concat(
            separatingAxisCandidates(polygon),
            polygon.vertices().stream().map(v -> circle.center().subtract(v)).toList());

    return resolution(polygon, circle, separatingAxisCandidates);
  }

  public static Vector2D resolution(Circle circle, Polygon polygon) {
    return resolution(polygon, circle).scale(-1);
  }

  // Returns the minimum translation vector to move the second shape such that it no longer overlaps
  // the first.
  private static Vector2D resolution(
      Shape first, Shape second, Iterable<Vector2D> separatingAxisCandidates) {
    Vector2D resolution = null;

    for (Vector2D candidate : separatingAxisCandidates) {
      Range firstProjection = first.scalarProjection(candidate);
      Range secondProjection = second.scalarProjection(candidate);
      Range intersection = firstProjection.intersect(secondProjection);

      if (intersection.length() == 0.0) {
        return Vector2D.ZERO;
      }

      if (resolution == null || intersection.length() < resolution.magnitude()) {
        int direction = firstProjection.start() < secondProjection.start() ? 1 : -1;
        resolution = candidate.unit().scale(intersection.length() * direction);
      }
    }

    // Must not have been any candidates, or else the method would have already returned or
    // the resolution would be non-null.
    if (resolution == null) {
      throw new IllegalArgumentException();
    }

    return resolution;
  }
}
