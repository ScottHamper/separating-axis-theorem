package com.scotthamper.sat.model;

public interface InputProvider {
  Vector2D movement();
}
