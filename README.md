Separating Axis Theorem
=======================
A basic demo of detecting and resolving collisions between convex shapes.

![](screenshot.png)

The blue-filled polygon is an immovable obstacle that the hollow shapes collide against. The hollow
shapes are movable via some basic keyboard controls:
- Arrow keys: move the circle.
- W, A, S, D: move the polygon.

How the Theorem Works
---------------------
The separating axis states that, if we can find an axis in which the projections of two convex
shapes onto that axis do not overlap, then those shapes must not be colliding; if the projections
don't overlap, then there must be a line that separates the two shapes. The trick then, is to figure
out what axes should be tested, as there are infinitely many.

When it comes to polygons, we only need to test against the axes that are perpendicular to each edge
of each polygon. For testing a circle against a polygon, it gets a little weirder since circles
don't have edges, but do have infinitely many tangent lines (and thus, infinitely many axes
perpendicular to those tangent lines). However, we only need test against the axes that are parallel
to the lines drawn from the center of the circle to each vertex of the polygon (in addition to the
axes perpendicular to each edge of the polygon, as described previously).

If we test all candidate axes and none prove to separate the shapes, then the shapes are colliding.

Additional Resources
--------------------
- [SAT (Separating Axis Theorem)](https://dyn4j.org/2010/01/sat/)
- [Hyperplane Separation Theorem](https://en.wikipedia.org/wiki/Hyperplane_separation_theorem)